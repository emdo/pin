---
layout: post
title:  about_participation
language: en
categories: about
header: Participation
---

We understand participation to mean the active co-determination and participation in decision-making processes. This includes the contribution of ideas as well as the participation in projects, events and dialogue. One of our objectives is to encourage the cooperation with individuals, associations, organization and institutions. Thereby we focus on the attempt to continually speak to each other instead of for others. 
