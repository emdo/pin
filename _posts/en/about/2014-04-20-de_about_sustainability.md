---
layout: post
title:  about_sustainability
language: en
categories: about
header: Sustainability
---

We take sustainability to mean the combination of foresight and continuity. With our projects we aim at long-lasting effects and achieving effective problem-solving approaches through forward-looking thinking and action. We are convinced that social and structural changes cannot be achieved overnight, but through continuous work. 
