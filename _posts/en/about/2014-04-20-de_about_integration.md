---
layout: post
title:  about_integration
language: en
categories: about
header: Integration
---

For *pin* integration, especially sustainable integration stands for a dynamic, long-lasting and very differentiated process of growing together. This very process must be ensured by promoting the following points:

- multidimensional communication
- convergence and acceptance of shared responsibilities, which represent improvements for everyone
- awareness for the “integration instead of assimilation” principle
- solidarity and understanding for the topic escape and migration as well as according and relevant coherences
- “participate and let participate”
- integration in the educational sector and labor market
- equal opportunities and support through information and counseling 
