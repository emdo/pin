---
layout: post
title:  about_integration
language: de
categories: about
header: Integration
---

pin – Verein für Partizipation, Integration & Nachhaltigkeit sieht Integration, insbesondere nachhaltige Integration, als dynamischen, lange andauernden und sehr differenzierten Prozess des „Zusammenwachsens“. Dieser Prozess muss durch folgende Punkte gesichert werden:

- Mehrdimensionale Kommunikation
- Allen zu Gute kommende Annäherung und Übernahme gemeinschaftlicher Verantwortung
- Bewusstsein für den „Integration statt Assimilation“-Grundsatz
- Solidarität und Verständnis für das Thema Flucht und Migration sowie entsprechende Zusammenhänge
- „Teilhaben und teilhaben lassen“
- Integration im Sprach- und Bildungssektor sowie Arbeitsmarktintegration
- Chancengleichheit und Unterstützung durch Information und Beratung
