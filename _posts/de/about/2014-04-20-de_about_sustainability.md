---
layout: post
title:  about_sustainability
language: de
categories: about
header: Nachhaltigkeit
---

Unter Nachhaltigkeit verstehen wir die Kombination von Voraussicht und Kontinuität. Mit unseren Projekten versuchen wir eine langfristige Wirkung zu erzielen und durch vorausschauendes Denken und Handeln, effektive Lösungsansätze zu erreichen. Wir sind davon überzeugt, dass gesellschaftliche und strukturelle Veränderung nicht von heute auf morgen passiert, sondern durch kontinuierliche Arbeit.
