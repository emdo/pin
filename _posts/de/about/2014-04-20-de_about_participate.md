---
layout: post
title:  about_participation
language: de
categories: about
header: Partizipation
---

Unter Partizipation verstehen wir die aktive Mitgestaltung und Teilnahme an Entscheidungsprozessen. Dabei geht es sowohl um das Einbringen von Ideen sowie die Beteiligung an Projekten, Veranstaltungen und Dialogen. Eines unserer Anliegen ist es die Zusammenarbeit mit Einzelpersonen, Vereinen, Organisationen und Institutionen zu fördern. Dabei steht der Versuch, stets miteinander, anstatt für andere zu sprechen für uns an höchster Stelle.
